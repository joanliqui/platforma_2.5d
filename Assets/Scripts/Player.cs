﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody rb;
    private Vector3 axis;
    [SerializeField] float playerSpeed;
    [SerializeField] float jumpForce;
    private bool jumping = false;
    private bool secondJump = false;
    [SerializeField] float secondJumpForce;


    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void SetAxis(Vector3 input){
        axis = input;
    }

private void FixedUpdate() {

    //Movimiento Horizonatl
    if(Mathf.Abs(rb.velocity.z) < 30){
    rb.velocity = new Vector3(0, rb.velocity.y, axis.z * -playerSpeed );
    }
    //Movimiento Vertical
    if (Input.GetButtonDown("Jump")){
        if(!jumping && !secondJump){
                jumping = true;
                Jump();
            }
        else if (jumping && !secondJump){
            secondJump = true;
            SecondJump();
        }
        }
    }

    void Jump(){
        rb.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
    }

     void SecondJump(){
        rb.AddForce(Vector3.up * secondJumpForce*2, ForceMode.VelocityChange);
    }

     private void OnCollisionEnter(Collision other) {
        jumping = false;
        secondJump = false;
    }
}   